FROM node:14.18.1-alpine

WORKDIR /app

COPY . .
RUN chown -R node:node .

USER node

RUN npm install

CMD npm start
