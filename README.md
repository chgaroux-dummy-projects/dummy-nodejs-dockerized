# Dummy Node.js dockerized

## App

**Run** :
```shell script
npm start
```

## Docker
**Build** :
```shell script
docker build . -t dummy-nodejs
```
